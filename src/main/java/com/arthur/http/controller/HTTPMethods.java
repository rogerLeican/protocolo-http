package com.arthur.http.controller;

import com.arthur.http.service.Pessoa;
import com.arthur.http.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;



@RestController
@RequestMapping(value = "/http")
public class HTTPMethods {

    @Autowired
    PessoaService pessoaService;

    @GetMapping
    public List<Pessoa> todasPessoas() {
        return pessoaService.encontrarTodos();

    }

    @GetMapping("/{id}")
    public ResponseEntity<Pessoa> byID(@PathVariable(value = "id") Integer id) {
        Pessoa pessoa = pessoaService.encontrarPorID(id).get();

        return ResponseEntity.ok().body(pessoa);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public List<Pessoa> criarPessoa() {
        return pessoaService.salvarPessoa();
    }


    @DeleteMapping("/{id}")
    public void deletarPessoa(@PathVariable(value = "id") Integer id) {
        pessoaService.deletarPessoa(id);
    }


    @PutMapping("/{id}")//todo estava com ] <==(colchete) em vez de }(chave) no final do valor ex: /{id] <==
    public Pessoa atualizarPessoa(@PathVariable(value = "id") Integer id, @RequestBody Pessoa pessoaDetalhe) {
        Pessoa pessoa = pessoaService.encontrarPorID(id).get();

        pessoa.setName(pessoaDetalhe.getName());
        pessoa.setId(pessoaDetalhe.getId());
        pessoa.setEmail(pessoaDetalhe.getEmail());

        final Pessoa pessoaAtualizada = (Pessoa) pessoaService.salvar(pessoa);
        return pessoaService.atualizarPessoa(id, pessoaAtualizada);
    }


    @PatchMapping
    public @ResponseBody ResponseEntity<String> patch() {
        return new ResponseEntity<String>("PATCH Responde", HttpStatus.OK);
    }
}