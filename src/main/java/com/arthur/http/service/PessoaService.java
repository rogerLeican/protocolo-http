package com.arthur.http.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PessoaService {

    Pessoa pessoa = new Pessoa();
    Pessoa pessoa1 = new Pessoa();

    @Autowired
    PessoaRepository pessoaRepository;


    public List<Pessoa> salvarPessoa() {
        pessoa.setEmail("arthur@gmail.com");
        pessoa.setId(1);
        pessoa.setName("Arthur");

        pessoaRepository.save(pessoa);

        pessoa1.setEmail("maria@gmail.com");
        pessoa1.setId(2);
        pessoa1.setName("Maria");

        pessoaRepository.save(pessoa1);

        return pessoaRepository.findAll();
    }




    public List<Pessoa> salvar(Pessoa pessoa) {

        pessoaRepository.save(pessoa);

        return pessoaRepository.findAll();
    }

    public List<Pessoa> encontrarTodos() {
        return pessoaRepository.findAll();
    }

    public Pessoa atualizarPessoa(Integer id, Pessoa pessoa) {
        pessoa.setId(id);

        return pessoaRepository.save(pessoa);
    }

    public void deletarPessoa(Integer id) {

        pessoaRepository.existsById(id);
    }

    public Optional<Pessoa> encontrarPorID(Integer id) {
        return pessoaRepository.findById(id);
    }
}
