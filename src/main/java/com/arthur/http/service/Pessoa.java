package com.arthur.http.service;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
//todo faltou colocar o @id e @Document
@Data
@Document
public class Pessoa {
    @Id
    private Integer id;
    private String name;
    private String email;
}